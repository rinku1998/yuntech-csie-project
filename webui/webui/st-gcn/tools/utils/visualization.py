import cv2
import numpy as np


def stgcn_visualize(pose,
                    edge,
                    feature,
                    video,
                    label=None,
                    label_sequence=None,
                    height=1080):

    _, T, V, M = pose.shape  # T=frame數量 , V=節點數量(coco=18), M=人數
    T = len(video)
    pos_track = [None] * M
    for t in range(T):
        frame = video[t]

        # image resize
        H, W, c = frame.shape
        frame = cv2.resize(frame, (height * W // H // 2, height//2))
        H, W, c = frame.shape
        scale_factor = 2 * height / 1080

        # draw skeleton 畫骨架
        skeleton = frame * 0
        text = frame * 0
        for m in range(M):
            score = pose[2, t, :, m].mean()
            if score < 0.3:
                continue

            for i, j in edge:
                xi = pose[0, t, i, m]
                yi = pose[1, t, i, m]
                xj = pose[0, t, j, m]
                yj = pose[1, t, j, m]
                if xi + yi == 0 or xj + yj == 0:
                    continue
                else:
                    xi = int((xi + 0.5) * W)
                    yi = int((yi + 0.5) * H)
                    xj = int((xj + 0.5) * W)
                    yj = int((yj + 0.5) * H)
                cv2.line(skeleton, (xi, yi), (xj, yj), (255, 255, 255),
                         int(np.ceil(2 * scale_factor)))

            body_label = label_sequence[t // 4][m]
            x_nose = int((pose[0, t, 0, m] + 0.5) * W)
            y_nose = int((pose[1, t, 0, m] + 0.5) * H)
            x_neck = int((pose[0, t, 1, m] + 0.5) * W)
            y_neck = int((pose[1, t, 1, m] + 0.5) * H)

            half_head = int(((x_neck - x_nose)**2 + (y_neck - y_nose)**2)**0.5)
            pos = (x_nose + half_head, y_nose - half_head)
            if pos_track[m] is None:
                pos_track[m] = pos
            else:
                new_x = int(pos_track[m][0] + (pos[0] - pos_track[m][0]) * 0.2)
                new_y = int(pos_track[m][1] + (pos[1] - pos_track[m][1]) * 0.2)
                pos_track[m] = (new_x, new_y)
            cv2.putText(text, body_label, pos_track[m],
                        cv2.FONT_HERSHEY_TRIPLEX, 0.5 * scale_factor,
                        (255, 255, 255))

        # generate mask
        mask = frame * 0
        feature = np.abs(feature)
        feature = feature / feature.mean()
        for m in range(M):
            score = pose[2, t, :, m].mean()
            if score < 0.3:
                continue

            f = feature[t // 4, :, m]**5
            if f.mean() != 0:
                f = f / f.mean()
            for v in range(V):
                x = pose[0, t, v, m]
                y = pose[1, t, v, m]
                if x + y == 0:
                    continue
                else:
                    x = int((x + 0.5) * W)
                    y = int((y + 0.5) * H)
                cv2.circle(mask, (x, y), 0, (255, 255, 255),
                           int(np.ceil(f[v]**0.5 * 8 * scale_factor)))
        blurred_mask = cv2.blur(mask, (12, 12))

        skeleton_result = blurred_mask.astype(float) * 0.75
        skeleton_result += skeleton.astype(float) * 0.25
        skeleton_result += text.astype(float)
        skeleton_result[skeleton_result > 255] = 255
        skeleton_result.astype(np.uint8)

        rgb_result = blurred_mask.astype(float) * 0.75
        rgb_result += frame.astype(float) * 0.5
        rgb_result += skeleton.astype(float) * 0.25
        rgb_result[rgb_result > 255] = 255
        rgb_result.astype(np.uint8)

        put_text(skeleton, 'inputs of st-gcn', (0.1, 0.5))

        text_1 = cv2.imread(
            './resource/demo_asset/original_video.png', cv2.IMREAD_UNCHANGED)
        text_2 = cv2.imread(
            './resource/demo_asset/pose_estimation.png', cv2.IMREAD_UNCHANGED)
        text_3 = cv2.imread(
            './resource/demo_asset/attention+prediction.png', cv2.IMREAD_UNCHANGED)
        text_4 = cv2.imread(
            './resource/demo_asset/attention+rgb.png', cv2.IMREAD_UNCHANGED)

        blend(frame, text_1)
        blend(skeleton, text_2)
        blend(skeleton_result, text_3)
        blend(rgb_result, text_4)

        if label is not None:
            label_name = 'voting result: ' + label
            put_text(skeleton_result, label_name, (0.1, 0.5))

        img0 = np.concatenate((frame, skeleton), axis=1)
        img1 = np.concatenate((skeleton_result, rgb_result), axis=1)
        img = np.concatenate((img0, img1), axis=0)

        yield img


def put_text(img, text, position, scale_factor=1):
    t_w, t_h = cv2.getTextSize(
        text, cv2.FONT_HERSHEY_TRIPLEX, scale_factor, thickness=1)[0]
    H, W, _ = img.shape
    position = (int(W * position[1] - t_w * 0.5),
                int(H * position[0] - t_h * 0.5))
    params = (position, cv2.FONT_HERSHEY_SIMPLEX, scale_factor,
              (255, 255, 255), 2, cv2.LINE_AA)
    cv2.putText(img, text, *params)


def blend(background, foreground, dx=20, dy=10, fy=0.7):

    foreground = cv2.resize(foreground, (0, 0), fx=fy, fy=fy)
    h, w = foreground.shape[:2]
    b, g, r, a = cv2.split(foreground)
    mask = np.dstack((a, a, a))
    rgb = np.dstack((b, g, r))

    canvas = background[-h-dy:-dy, dx:w+dx]
    imask = mask > 0
    canvas[imask] = rgb[imask]


def stgcn_visualize_from_frame(pose,
                               edge,
                               video,
                               label=None,
                               label_sequence=None,
                               height=1080):

    _, T, V, M = pose.shape
    print(pose.shape)
    print(label)
    print(len(video))

    '''
    T = Pose frame數量
    V = 節點數量(coco=18)
    M = 人數
    '''
    # T = len(video)
    pos_track = [None] * M

    for t in range(T):
        print(t)
        frame = video[t]

        # 取得frame的長寬
        H, W, c = frame.shape

        # draw skeleton 畫骨架
        for m in range(M):

            # 檢查每個座標點acc是不是有大於0.3(30%)
            score = pose[2, t, :, m].mean()
            if score < 0.3:
                continue

            for i, j in edge:
                # (x1, y1)
                x1 = pose[0, t, i, m]
                y1 = pose[1, t, i, m]
                # (x2, y2)
                x2 = pose[0, t, j, m]
                y2 = pose[1, t, j, m]

                # 避開自己連自己
                if x1 + y1 == 0 or x2 + y2 == 0:
                    continue

                else:
                    x1 = int((x1 + 0.5) * W)
                    y1 = int((y1 + 0.5) * H)
                    x2 = int((x2 + 0.5) * W)
                    y2 = int((y2 + 0.5) * H)
                cv2.line(frame, (x1, y1), (x2, y2), (255, 255, 255), 2)

            # 每個frame的label
            # 計算label顯示位置
            body_label = label_sequence[t // 4][m]
            x_nose = int((pose[0, t, 0, m]+0.5) * W)
            y_nose = int((pose[1, t, 0, m]+0.5) * H)
            x_neck = int((pose[0, t, 1, m]+0.5) * W)
            y_neck = int((pose[1, t, 1, m]+0.5) * H)

            half_head = int(((x_neck - x_nose)**2 + (y_neck - y_nose)**2)**0.5)
            pos = (x_nose + half_head, y_nose - half_head)
            if pos_track[m] is None:
                pos_track[m] = pos
            else:
                new_x = int(pos_track[m][0] + (pos[0] - pos_track[m][0]) * 0.2)
                new_y = int(pos_track[m][1] + (pos[1] - pos_track[m][1]) * 0.2)
                pos_track[m] = (new_x, new_y)
            cv2.putText(frame, body_label, pos_track[m],
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5 * 2,
                        (255, 255, 255), 2, cv2.LINE_AA)

        # 整部預測結果
        if label is not None:
            label_name = 'Predict Result: ' + label
            put_text(frame, label_name, (0.1, 0.5))

        img = frame
        yield img

#Put action label on video (5 frame)
def stgcn_visualize_from_five_frame(pose,
                                    edge,
                                    video,
                                    label=None,
                                    label_sequence=None,
                                    height=1080,
                                    frame_segment=5):

    _, T, V, M = pose.shape
    T = len(video)  # 取得影片frame數

    '''
    T = Pose frame數量
    V = 節點數量(coco=18)
    M = 人數
    '''

    pos_track = [None] * M
    temp_pos_track = [()]*M
    temp_person_area = []
    previos_person_area = []
    for _ in range(M):
        
        temp_person_area.append([(None, None),(None, None)])
        previos_person_area.append([(None, None),(None, None)])
        
        # temp_person_area.append([[None, None],[None, None]])
        # previos_person_area.append([[None, None],[None, None]])
    frames = []
    action_label=''   
    for t in range(T):

        frame = video[t]

        # 取得frame的長寬
        H, W, c = frame.shape

        # 畫人物位置
        for m in range(M):

            # 檢查每個座標點acc是不是有大於0.3(30%)
            score = pose[2, t, :, m].mean()
            if score < 0.3:
                continue

            if t % frame_segment == 0:
                tt = t//frame_segment
                # 計算label名稱 
                if t % 4 == 0:
                    action_label = label_sequence[t // frame_segment //4][m]

                # 計算人物框
                top = 1 #pose[1, tt, 0, m]
                bottom = -1 #pose[1, tt, 0, m]
                left = 1 #pose[0, tt, 0, m]
                right = -1 #pose[0, tt, 0, m]

                for v in range(V):
                    if not (pose[0, tt, v, m] == 0.0 or pose[1, tt, v, m] == 0.0 ):
                        if(pose[1, tt, v, m]<top):
                            top = pose[1, tt, v, m]
                        if(pose[1, tt, v, m]>bottom):
                            bottom = pose[1, tt, v, m]
                        if(pose[0, tt, v, m]<left):
                            left = pose[0, tt, v, m]
                        if(pose[0, tt, v, m]>right):
                            right = pose[0, tt, v, m]
                
                temp_person_area[m][0] = (int((left+0.5)*W*0.95) ,int((top+0.5)*H*0.95))
                temp_person_area[m][1] = (int((right+0.5)*W*1.05) ,int((bottom+0.5)*H*1.05))

                '''
                temp_person_area[m][0] = [int((left+0.5)*W*0.9) ,int((top+0.5)*H*0.9)]
                temp_person_area[m][1] = [int((right+0.5)*W*1.1) ,int((bottom+0.5)*H*1.1)]
                
                if t==0:
                    previos_person_area[m][0] = temp_person_area[m][0]
                    previos_person_area[m][1] = temp_person_area[m][1]
                if t>0:
                    temp_person_area[m][0][0] = int(0.9*temp_person_area[m][0][0] + 0.1*previos_person_area[m][0][0])
                    temp_person_area[m][0][1] = int(0.9*temp_person_area[m][0][1] + 0.1*previos_person_area[m][0][1])
                    temp_person_area[m][1][0] = int(0.9*temp_person_area[m][1][0] + 0.1*previos_person_area[m][1][0])
                    temp_person_area[m][1][1] = int(0.9*temp_person_area[m][1][1] + 0.1*previos_person_area[m][1][1])
                    previos_person_area[m][0] = temp_person_area[m][0]
                    previos_person_area[m][1] = temp_person_area[m][1]
                '''
            #print(temp_person_area[m])
            cv2.putText(frame, action_label, (temp_person_area[m][0][0], int(temp_person_area[m][0][1])-10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        (255, 255, 255), 1, cv2.LINE_AA)
            cv2.rectangle(frame, temp_person_area[m][0], temp_person_area[m][1], (255, 255, 255), 2)
            # print(t, m , temp_person_area[m][0], temp_person_area[m][1])

        # 整部預測結果
        if label is not None:
            label_name = 'Predict Result: ' + label
            put_text(frame, label_name, (0.1, 0.5))

        # img = frame
        # yield img
        frames.append(frame)
    return frames
