from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import SubmitField

class VideoForm(FlaskForm):
    video = FileField(validators=[FileRequired(u'請選擇影片')], render_kw={'style': 'display:none'})
    submit = SubmitField(u'上傳', render_kw={'class': 'btn btn-jelly normal-btn loading'})