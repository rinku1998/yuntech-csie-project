import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""
import imutils
import pickle
import cv2
import dlib
import gc
import numpy as np
from numba import cuda
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from keras import backend as K

class VideoCamera():
    def __init__(self, path):
        self.detector = dlib.get_frontal_face_detector()
        self.model = load_model(path)
        self.emos = ['neutral', 'happy', 'sad', 'suprise', 'angry', 'fear', 'disgust']
        self.count = 0
        self.template = {'x1': None,'x2': None,'y1': None,'y2': None,'emo': None}
        self.pfs = []
        self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

    def __del__(self):
        pass
        # self.cap.release()
        # K.clear_session()
        # del self.model
        # gc.collect()
        # print('entered del')
        # cuda.select_device(0)
        # cuda.close()

    def get_frame(self):
        ret, frame = self.cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces, scores, idx = self.detector.run(gray, 0)

        if self.count % 3 == 0 :
            self.pfs = []
            for i, d in enumerate(faces):
                x1 = d.left()
                y1 = d.top()
                x2 = d.right()
                y2 = d.bottom()

                # 圖片預處理
                face = frame[y1:y2, x1:x2]
                if x1 < 0 or x2 < 0 or y1 < 0 or y2 < 0:
                    continue
                face = cv2.resize(face, (48, 48))
                face = face.astype('float') / 255.0
                face = img_to_array(face)
                face = np.expand_dims(face, axis=0)

                # 表情分類
                proba = self.model.predict(face)[0]
                idx = np.argmax(proba)
                label = self.emos[idx]
                cloned = self.template.copy()
                cloned['x1'] = x1
                cloned['x2'] = x2
                cloned['y1'] = y1
                cloned['y2'] = y2
                cloned['emo'] = label
                self.pfs.append(cloned)
                # 框臉並標示分類結果
                cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, label, (x1, y1-10), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1, cv2.LINE_AA)
        else:
            for pf in self.pfs:
                cv2.rectangle(frame, (pf['x1'],pf['y1']), (pf['x2'], pf['y2']), (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, pf['emo'], (pf['x1'], pf['y1']-10), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1, cv2.LINE_AA)  
        self.count += 1

        _, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()       
