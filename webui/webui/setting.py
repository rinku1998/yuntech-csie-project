import os

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


class BaseConfig(object):
    SECRET_KEY = os.getenv('SECRET_KEY', 'dev key')

    UPLOADED_FACIALS_DEST = os.path.join(basedir, 'webui/static/videos/facials')
    UPLOADED_ACTIONS_DEST = os.path.join(basedir, 'webui/static/videos/actions')


class DevelopmentConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    pass


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}

