import sys
import cv2
import os
import json
import tools
import tools.utils as utils
from sys import platform

def json_pack(skeleton_npary, frame_width, frame_height, frame_id):
    skeleton_ary = skeleton_npary.tolist()
    num_person = skeleton_npary.shape[0]
    num_keypoint = skeleton_npary.shape[1]
    frame_data = {'frame_index': frame_id}
    skeletons = []
    for person in range(num_person):
        score, coordinates = [], []
        skeleton = {}
        # print('564')
        keypoints = skeleton_ary[person]
        
        for i in range(num_keypoint):
            coordinates += [keypoints[i][0]/frame_width, keypoints[i][1]/frame_height]
            score += [keypoints[i][2]]

        skeleton['pose'] = coordinates
        skeleton['score'] = score
        skeletons += [skeleton]

    frame_data['skeleton'] = skeletons
    return frame_data

def calculateSkeleton(video_name, label, label_index):
    try:
        # Starting OpenPose
        opWrapper = op.WrapperPython()
        opWrapper.configure(params)
        opWrapper.start()

        # Process Image
        datum = op.Datum()

        # Video Setting
        cap = cv2.VideoCapture(video_name)

        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))   # float
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) # float
        frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        # print(width)
        count = 0
        sequence_info = []
        while(cap.isOpened()):

            ret, frame = cap.read()
            if ret is True:
                # print('Now in frame ',count)
                datum = op.Datum()
                datum.cvInputData = frame
                opWrapper.emplaceAndPop([datum])
                # print(datum.poseKeypoints)

                # Check if skeleton is detected
                if(datum.poseKeypoints.size > 1):
                    frame_data = json_pack(datum.poseKeypoints, width, height, count)
                    count+=1
                    sequence_info += [frame_data]

            else:
                break

        cap.release()
        video_info = dict()
        video_info['data'] = sequence_info
        video_info['label'] = label
        video_info['label_index'] = label_index
        return video_info

    except Exception as e:
        print(e)
        sys.exit(-1)

if __name__ == "__main__":
    '''
    初始資料夾設定
    st-gcn/
    ├─dataset/
    │  ├─train_data
    │  │  ├─jump up
    │  │  ├─sit down
    │  │  └─stand up
    │  └─val_data
    │      ├─jump up
    │      ├─sit down
    │      └─stand up
    └─make_skeleton.py

    Run by python3 main.py demo --openpose ~/openpose/build/ --video <video_directory> --config ./config/st_gcn/kinetics-skeleton/NTU-4.yaml
    '''

    # Import Openpose Libraries
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        # Windows Import
        if platform == "win32":
            # Change these variables to point to the correct folder (Release/x64 etc.) 
            sys.path.append(dir_path + '/../../python/openpose/Release');
            os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../../x64/Release;' +  dir_path + '/../../bin;'
            import pyopenpose as op
        else:
            # Change these variables to point to the correct folder (Release/x64 etc.) 
            sys.path.append('~/openpose/build/python');
            # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
            # sys.path.append('/usr/local/python')
            from openpose import pyopenpose as op
    except ImportError as e:
        print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    # Openpose 設定
    params = dict()
    params["model_folder"] = "/root/openpose/models/"
    params["model_pose"] = "COCO"
    # params["write_json"] = "coco_18_keypoint.json"
    params["num_gpu"] = 1
    
    # Load videos from each category
    datasets_dir = 'dataset/' # 訓練集、驗證集影片資料夾位置
    datasets = os.listdir(datasets_dir)
    skeleton_output_dir = 'skeleton_dataset/' # 輸出路徑
    label_output_dir = 'label_name.txt' #標籤檔(辨識用)

    for dataset in datasets:

        # 初始化儲存變數
        label = {}
        label_index = 0

        # 檢查輸出目錄是否有train和val資料夾，若沒有則新建
        if not os.path.isdir(os.path.join(skeleton_output_dir, 'packed/', dataset)):
            os.makedirs(os.path.join(skeleton_output_dir, 'packed/', dataset))

        print('Now processing is ', dataset)
        dataset_dir = os.listdir(os.path.join(datasets_dir, dataset))
        for category in dataset_dir:
            category_dir = os.path.join(datasets_dir, dataset, category)
            if os.path.isdir(category_dir):
                videos = os.listdir(category_dir)
                video_count = 1
                for video in videos:

                    # 整理輸出路徑
                    video_name = video.split('.')[0]
                    video_dir = os.path.join(os.getcwd(), category_dir, video)
                    # 執行Openpose，取得每個video的skeleton
                    print('Detecting skeleton in video->', video_dir)
                    video_info = calculateSkeleton(video_dir, category, label_index)
                    # 設定包好的video skeleton路徑並寫入json檔
                    skeleton_pack_dir = os.path.join(skeleton_output_dir, 'packed/', dataset)
                    json_filename = skeleton_pack_dir + '/' + video_name + '.json'
                    print('Video json will be saved in ', json_filename)
                    with open(json_filename, 'w') as outfile:
                        json.dump(video_info, outfile)

                    # 加入標籤json檔
                    label[video_name] = {'has_skeleton': True, 'label': category, 'label_index': label_index}
                    
                    print('Complete: ',video_count, '/',len(videos))
                    video_count += 1
        
            label_index += 1

        label_filename = os.path.join(skeleton_output_dir, 'packed/') + dataset + '_label.json'
        with open(label_filename, 'w') as w:
            json.dump(label, w)

with open(label_output_dir, 'w') as w:
    label_name = os.listdir(datasets[0])
    for l in label_name:
        w.writeline(l + '\n')
