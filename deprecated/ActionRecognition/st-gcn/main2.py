#!/usr/bin/env python
import argparse
import sys

# torchlight
import torchlight
from torchlight import import_class
import time

def load_stgcn_model(config, video_path):
    
    # CONFIG_FILE = './config/st_gcn/childrenAct/childrenAct_recognize_new.yaml'
    CONFIG_FILE = config

    # Loading model
    print('-Start loading ST-GCN model')
    process_arg = ['--config', CONFIG_FILE, '--video', video_path]
    
    a = time.time()
    parser = argparse.ArgumentParser(description='Processor collection')

    # region register processor yapf: disable
    processors = dict()
    processors['recognition'] = import_class('processor.recognition.REC_Processor')
    processors['demo'] = import_class('processor.demo.Demo')
    #endregion yapf: enable

    Processor = processors['demo']
    p = Processor(process_arg)

    b = time.time()
    print('Model load time %.2fs' %(b-a))
    print('--------------------------')
    return p
    
def start_recognition(process, frame_segment):
    process.start_frame_all_initial(frame_segment)

if __name__ == '__main__':
    '''
    a = time.time()
    parser = argparse.ArgumentParser(description='Processor collection')

    # region register processor yapf: disable
    processors = dict()
    processors['recognition'] = import_class('processor.recognition.REC_Processor')
    processors['demo'] = import_class('processor.demo.Demo')
    #endregion yapf: enable

    # add sub-parser
    subparsers = parser.add_subparsers(dest='processor')
    for k, p in processors.items():
        subparsers.add_parser(k, parents=[p.get_parser()])

    # read arguments
    arg = parser.parse_args()

    # start
    Processor = processors[arg.processor]
    p = Processor(sys.argv[2:])


    b = time.time()
    print('Model load time %.2fs' %(b-a))
    print('--------------------------')
    # p.start()
    p.start_frame()
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='./config/st_gcn/childrenAct/childrenAct_recognize_new.yaml',type=str)
    parser.add_argument('--video', default='/root/st-gcn/test_video/cut2.mp4',type=str)
    parser.add_argument('--frame_segment', default=5, type=int)
    args = parser.parse_args()
    p = load_stgcn_model(args.config, args.video)
    start_recognition(p, args.frame_segment)

