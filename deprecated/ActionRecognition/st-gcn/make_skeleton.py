import os
import json
import tools
import tools.utils as utils

# 路徑設定
datasets_dir = 'dataset/'
datasets = os.listdir(datasets_dir)
skeleton_output_dir = 'skeleton_dataset/'
openpose_path = '/root/openpose/build//examples/openpose/openpose.bin'


def pack(video_dir, skeleton_snippest_dir, video_name, label_index, dataset, category):
    video = utils.video.get_video_frames(video_dir) #將video每個frame轉成list
    height, width, _ = video[0].shape #取得影片的長寬
    video_info = utils.openpose.json_pack( #從json檔整理資訊(x, y, acc等)
        skeleton_snippest_dir, video_name, width, height, category, label_index)

    skeleton_pack_dir = os.path.join(skeleton_output_dir, 'packed/', dataset)
    json_filename = skeleton_pack_dir + '/' + video_name + '.json'
    print(json_filename)
    with open(json_filename, 'w') as outfile:
        json.dump(video_info, outfile)
    if len(video_info['data']) == 0: #沒找到骨架(openpose輸出資訊)
        print('Can not find pose estimation results.')
        return
    else:
        print('Pose estimation complete.')

for dataset in datasets:

    # 初始化儲存變數
    label = {}
    label_index = 0

    # 檢查輸出目錄是否有train和val資料夾，若沒有則新建
    if not os.path.isdir(os.path.join(skeleton_output_dir, 'packed/', dataset)):
        os.makedirs(os.path.join(skeleton_output_dir, 'packed/', dataset))

    print('Now calculating', dataset)
    dataset_dir = os.listdir(os.path.join(datasets_dir, dataset))
    for category in dataset_dir:
        category_dir = os.path.join(datasets_dir, dataset, category)
        if os.path.isdir(category_dir):
            videos = os.listdir(category_dir)
            for video in videos:

                # 整理輸出路徑
                video_name = video.split('.')[0]
                video_dir = os.path.join(category_dir, video)
                skeleton_snippest_dir = os.path.join(os.path.join(skeleton_output_dir,  'snippests/'), video_name)
                os.makedirs(skeleton_snippest_dir)

                # 執行Openpose
                print('Start Pose Estimation, count =', label_index)
                command_line = '{} --video {} --write_json {} --display 0 --render_pose 0 --model_pose COCO'.format(openpose_path, video_dir, skeleton_snippest_dir)
                os.system(command_line)
                pack(video_dir, skeleton_snippest_dir, video_name, label_index, dataset, category)

                label_index += 1
                label[video_name] = {'has_skeleton': True, 'label': category, 'label_index': label_index}
                print('Complete: ',label_index, '/',len(videos)*3)

    label_filename = os.path.join(skeleton_output_dir, 'packed/') + dataset + '_label.json'
    with open(label_filename, 'w') as w:
        json.dump(label, w)
