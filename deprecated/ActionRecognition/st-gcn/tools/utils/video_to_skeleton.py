# From Python
# It requires OpenCV installed for Python
import sys
import cv2
import os
from sys import platform
import random
import argparse
import json
import numpy as np
import time

# Import Openpose (Windows/Ubuntu/OSX)
dir_path = os.path.dirname(os.path.realpath(__file__))
try:
    # Windows Import
    if platform == "win32":
        # Change these variables to point to the correct folder (Release/x64 etc.)
        sys.path.append(
            dir_path + '/../../openpose/build/python/openpose/Release')
        os.environ['PATH'] = os.environ['PATH'] + ';' + dir_path + \
            '/../../openpose/build/x64/Release;' + dir_path + '/../../openpose/build/bin;'
        import pyopenpose as op
    else:
        # Change these variables to point to the correct folder (Release/x64 etc.)
        sys.path.append('/../../openpose/build/python')
        # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
        # sys.path.append('/usr/local/python')
        from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e


def compare(new_person, previous_person):
    check_valuesx = [100, 100, 100, 100, 100]
    check_valuesy = [100, 100, 100, 100, 100]
    # compare keypoint id
    check_keypoints = [1]  # ,4,7,10,13]
    for i, point in enumerate(check_keypoints):
        nxy_list = new_person[point]
        pxy_list = previous_person[len(previous_person) - 1][point]
        nx = nxy_list[0]
        ny = nxy_list[1]
        px = pxy_list[0]
        py = pxy_list[1]

        if check_valuesx[i] < abs(nx - px):
            return False
        if check_valuesy[i] < abs(ny - py):
            return False
    print('success')
    return True  # 不同人


def openpose_label(video_name):

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    # params["model_folder"] = "../../../models/"
    params["model_folder"] = "/root/openpose/models/"
    # params["image_dir"] = "images/"
    # params["net_resolution"] = "160x96"
    params["model_pose"] = "COCO"
    #params["write_json"] = "coco_18_keypoint.json"
    params["num_gpu"] = 1
    video = video_name
    try:
        # Starting OpenPose
        opWrapper = op.WrapperPython()
        opWrapper.configure(params)
        opWrapper.start()

        # Process Image
        datum = op.Datum()

        # Video Setting
        cap = cv2.VideoCapture(video)
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))   # float
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))  # float
        frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = cap.get(cv2.CAP_PROP_FPS)
        previous_id = []
        results = []
        count = 0

        fcount = 0
        while(cap.isOpened()):

            ret, frame = cap.read()
            if ret is True and fcount % 5 == 0:
                print('count', count)
                count += 1

                datum = op.Datum()
                datum.cvInputData = frame
                opWrapper.emplaceAndPop([datum])
                # print('size=',datum.poseKeypoints.size)
                points_around_head = [0, 14, 15, 16, 17]
                if datum.poseKeypoints.size >= 54:  # 大於一人
                    color_count = 0

                    next_size = len(datum.poseKeypoints)  # 人數
                    next_id = [-1 for _ in range(next_size)]  # 18個point
                    previous_flag = [False for _ in range(len(results))]
                    for i in range(next_size):  # 逐人檢查
                        for pid in previous_id:
                            if not previous_flag[pid] and compare(datum.poseKeypoints[i], results[pid]):
                                # match
                                previous_flag[pid] = True
                                next_id[i] = pid
                                # print('match')
                                break
                    # print('results')
                    for i, ID in enumerate(next_id):
                        valid = True
                        if ID == -1:  # 原本
                            xy_list = datum.poseKeypoints[i][1]
                            x = int(xy_list[0])
                            y = int(xy_list[1])

                            if x == 0 and y == 0:
                                valid = False
                            else:
                                new_id = len(results)
                                next_id[i] = new_id
                                results.append([])
                                ID = new_id
                        if valid:
                            results[ID].append(datum.poseKeypoints[i].tolist())

                    while -1 in next_id:
                        next_id.remove(-1)

                    previous_id = next_id

                print(len(results))
            elif ret is False:
                break
            fcount += 1
        cap.release()


        num_frame = len(results[0])
        frames = []

        return results
    except Exception as e:
        print(e)
        sys.exit(-1)

def openpose_label_no_sort(video, frame_segment=5):

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "/root/openpose/models/"
    params["model_pose"] = "COCO"
    # params["model_pose"] = "BODY_25B"
    # params["net_resolution"] = "-1x288"
    params["num_gpu"] = 1

    try:
        # Starting OpenPose
        opWrapper = op.WrapperPython()
        opWrapper.configure(params)
        opWrapper.start()

        # Process Image
        datum = op.Datum()
        num_frame = len(video)
        print('Total frame : ', num_frame)
        frame_count = 0
        results = []
        for frame in video:
            if frame_count % frame_segment == 0:           
                datum.cvInputData = frame
                opWrapper.emplaceAndPop([datum])
                results.append(datum.poseKeypoints)
            frame_count += 1
        return results

    except Exception as e:
        print(e)
        sys.exit(-1)



if __name__ == '__main__':
    a = time.time()
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--input', required=True,
                    help='path to a input video file')
    # ap.add_argument('-o', '--output', required=True, help='path to a output video file')
    args = vars(ap.parse_args())

    input_path = args['input']

    result = openpose_label(input_path)
    video_frame = np.array(result)
    b = time.time()
    print(b-a)
