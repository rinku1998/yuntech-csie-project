import skvideo.io
import numpy as np
import cv2

def video_info_parsing(video_info, num_person_in=5, num_person_out=4):
    data_numpy = np.zeros((3, len(video_info['data']), 18, num_person_in))
    for frame_info in video_info['data']:
        frame_index = frame_info['frame_index']
        for m, skeleton_info in enumerate(frame_info["skeleton"]):
            if m >= num_person_in:
                break
            pose = skeleton_info['pose']
            score = skeleton_info['score']
            data_numpy[0, frame_index, :, m] = pose[0::2]
            data_numpy[1, frame_index, :, m] = pose[1::2]
            data_numpy[2, frame_index, :, m] = score

    print(data_numpy[0,0,0,0])
    # centralization
    data_numpy[0:2] = data_numpy[0:2] - 0.5
    data_numpy[0][data_numpy[2] == 0] = 0
    data_numpy[1][data_numpy[2] == 0] = 0

    sort_index = (-data_numpy[2, :, :, :].sum(axis=1)).argsort(axis=1)
    for t, s in enumerate(sort_index):
        data_numpy[:, t, :, :] = data_numpy[:, t, :, s].transpose((1, 2,
                                                                    0))
    data_numpy = data_numpy[:, :, :, :num_person_out]

    label = video_info['label_index']
    return data_numpy, label

def video_info_from_skeleton(video_frame, height=0, width=0, num_person_in=5, num_person_out=2):

    num_frame = len(video_frame)
    max_person = 0
    print(num_frame)
    for frame_index in range(num_frame):
        if not video_frame[frame_index].shape == ():
            max_person = video_frame[frame_index].shape[0] if video_frame[frame_index].shape[0] > max_person else max_person
        # if video_frame[frame].shape[0] > max_person:
        #     max_person = len(video_frame[frame])

    data_numpy = np.zeros((3, num_frame, 18, max_person))
    print(data_numpy.shape)

    for frame in range(num_frame):
        if not video_frame[frame].shape == ():
            for person in range(video_frame[frame].shape[0]):
                # print(frame, '  ', person)
                for point in range(18):
                    data_numpy[0, frame, point, person] = video_frame[frame][person][point][0]/width
                    data_numpy[1, frame, point, person] = video_frame[frame][person][point][1]/height
                    data_numpy[2, frame, point, person] = video_frame[frame][person][point][2]
        
    # centralization
    data_numpy[0:2] = data_numpy[0:2] - 0.5
    data_numpy[0][data_numpy[2] == 0] = 0
    data_numpy[1][data_numpy[2] == 0] = 0

    sort_index = (-data_numpy[2, :, :, :].sum(axis=1)).argsort(axis=1)
    for t, s in enumerate(sort_index):
        data_numpy[:, t, :, :] = data_numpy[:, t, :, s].transpose((1, 2,
                                                                    0))
    data_numpy = data_numpy[:, :, :, :num_person_out]

    label = -1
    return data_numpy, label

def get_video_frames(video_path): #video -> frames(list)
    video = []
    # Load by skvideo
    
    vread = skvideo.io.vread(video_path)
        
    H, W, c = vread[0].shape
    '''
    # Load by OpenCV
    cap = cv2.VideoCapture(video_path)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))   # float
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) # float

    while(cap.isOpened()):
        ret, frame = cap.read()
        
        if height<=720 or width<=720:
            if ret is True:
                frame = frame[:,:,::-1]
                video.append(frame)
            elif ret is False:
                break
        else:
            if ret is True:
                frame = cv2.resize(frame, (int(width*720/height), 720), interpolation=cv2.INTER_CUBIC)
                frame = frame[:,:,::-1]
                video.append(frame)
            elif ret is False:
                break
    cap.release()
    '''
    # Force scale to 720px for height
    
    if H<=720 or W<=720:
        for frame in vread:
            video.append(frame)
    else:
        for frame in vread:
            frame = cv2.resize(frame, (int(W*720/H), 720), interpolation=cv2.INTER_CUBIC)
            video.append(frame)
    '''
    for frame in vread:
        video.append(frame)
    '''
    return video

def video_play(video_path, fps=30):
    cap = cv2.VideoCapture(video_path)

    while(cap.isOpened()):
        ret, frame = cap.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.imshow('frame',gray)
        if cv2.waitKey(1000/fps) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()