#!/usr/bin/env python
# coding: utf-8
import cv2
import csv
import os
import matplotlib.pyplot as plt
import numpy as np

import dlib
import imutils

def trans_to_jpg(ori_path, new_path, file_type):
    fs=os.listdir(ori_path)
    for f in fs:
        cmplt_path = os.path.join(ori_path, f)
        if not os.path.isdir(cmplt_path):
            img = cv2.imread(cmplt_path)
            cv2.imwrite(new_path + f[:-len(file_type)] + '.jpg', img)

def catch_face_show(img_path):
    img = cv2.imread(img_path)
    img = imutils.resize(img, width=400)
    
    detector = dlib.get_frontal_face_detector()
    catchface = detector(img, 0)
    
    for i,d in enumerate(catchface):
        x1 = d.left()
        y1 = d.top()
        x2 = d.right()
        y2 = d.bottom()

    cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 4, cv2.LINE_AA)
        
    # 顯示結果
    cv2.imshow("Face Detection", img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()    

def catch_face_new(ori_path, new_path):
    fs=os.listdir(ori_path)
    for f in fs:
        cmplt_path = os.path.join(ori_path, f)
        if not os.path.isdir(cmplt_path):
            
            img = cv2.imread(cmplt_path)
            img = imutils.resize(img, width=400)
            
            detector = dlib.get_frontal_face_detector()
            catchface = detector(img, 0)
            
            for i,d in enumerate(catchface):
                x1 = d.left()
                y1 = d.top()
                x2 = d.right()
                y2 = d.bottom()            
            
            face_img = img[y1:y2, x1:x2]
            face_img = cv2.resize(face_img, (48,48))
            cv2.imwrite(new_path + f, face_img)

def catch_oneface(img_path, new_path, name="t"):
    img = cv2.imread(img_path)
    img = imutils.resize(img, width=400)

    detector = dlib.get_frontal_face_detector()
    catchface = detector(img, 0)

    for i,d in enumerate(catchface):
        x1 = d.left()
        y1 = d.top()
        x2 = d.right()
        y2 = d.bottom()            

    face_img = img[y1:y2, x1:x2]
    face_img = cv2.resize(face_img, (48,48))
    cv2.imwrite(new_path + name + '.jpg', face_img)


#自我測試
'''
#trans_to_jpg('dataset\jaffe', 'dataset/jaffe_test/', '.tiff')

catch_face_show('mistermime.jpg')

catch_face_new('dataset\jaffe_test', 'dataset/jaffe_testf/')

catch_oneface('hi.jpg', '','hihi')
'''