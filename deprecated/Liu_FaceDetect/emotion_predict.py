import dlib
import cv2
import imutils

from keras.models import Model
from keras.models import load_model

import emotion_train as et


# 载入模型
model = load_model('my_model.h5')


#選擇第一隻攝影機
cap = cv2.VideoCapture(0)
#cap = cv2.VideoCapture('cc.mp4')
#調整預設影像大小，預設值很大，很吃效能
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 500)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 500)

#取得預設的臉部偵測器
detector = dlib.get_frontal_face_detector()

#當攝影機打開時，對每個frame進行偵測
while(cap.isOpened()):
#讀出frame資訊
    ret, frame = cap.read()

#偵測人臉
    face_rects, scores, idx = detector.run(frame, 0)

#取出偵測的結果
    print(face_rects)
    try:
        for i, d in enumerate(face_rects):
            x1 = d.left()
            y1 = d.top()
            x2 = d.right()
            y2 = d.bottom()
            print(x1,x2,y1,y2, sep = ' ')
            face_img = frame[y1:y2, x1:x2]
            face_img = cv2.resize(face_img, (48, 48))
            t=et.img_predict(0, face_img, (48, 48), model)
            tlist=['AN','DI','FE','HA','SA','SU','NE']
            text = tlist[t[0]]

            cv2.rectangle(frame, (x1, y1), (x2, y2), ( 0, 255, 0), 4, cv2. LINE_AA)
            cv2.putText(frame, text, (x1, y1), cv2. FONT_HERSHEY_DUPLEX,
            0.7, ( 0, 0, 255), 1, cv2. LINE_AA)
    except:
        pass
    cv2.imshow("Face Detection", frame)
    if cv2.waitKey( 10) == 27:
        break
#釋放記憶體
cap.release()
#關閉所有視窗
cv2.destroyAllWindows()




